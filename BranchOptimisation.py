#from dataWrapper1 import dataWrapper
import numpy as np
import pandas as pd
#from scipy.special import expit
import _pickle as pickle
#import pickle
from sklearn.metrics.pairwise import euclidean_distances
import itertools
#from scipy.optimize import differential_evolution
from sklearn.preprocessing import normalize
import re
from collections import defaultdict
import json
import ast


# Current State Inputs
try:
    filespath = "/home/ubuntu/deployment/NAB/"
    demos = pd.read_csv(filespath+"demos_parramatta_currentstate.csv")
    branch_traits = pd.read_csv(filespath+"branch_traits_parramatta_currentstate.csv",index_col = 'branch_id')
    media = pd.read_csv(filespath+"media_parramatta_currentstate.csv",index_col = 'branch_id')
    competitors = pd.read_csv(filespath+"competitorbranches_paramatta.csv")
    coeff_ft = pd.read_csv(filespath+"coeff_ft_parramatta.csv")
    coeff_rc = pd.read_csv(filespath+"coeff_rc_parramatta.csv")
    coeff_wt = pd.read_csv(filespath+"coeff_wt_parramatta.csv")
except:
    filespath = "/Users/us/Documents/work/scratch/kpmg/logins/deployment/NAB/"
    demos = pd.read_csv(filespath+"demos_parramatta_currentstate.csv")
    branch_traits = pd.read_csv(filespath+"branch_traits_parramatta_currentstate.csv",index_col = 'branch_id')
    media = pd.read_csv(filespath+"media_parramatta_currentstate.csv",index_col = 'branch_id')
    competitors = pd.read_csv(filespath+"competitorbranches_paramatta.csv")
    coeff_ft = pd.read_csv(filespath+"coeff_ft_parramatta.csv")
    coeff_rc = pd.read_csv(filespath+"coeff_rc_parramatta.csv")
    coeff_wt = pd.read_csv(filespath+"coeff_wt_parramatta.csv")


# PARRAMATTA Actual Lat-Long coordinate distances
with open(filespath+"parramatta_location_distances.pkl",'rb') as f:
    location_distances = pickle.load(f,encoding='bytes')

    
# Distance/Location scaling
location_distances = location_distances*1000
#location_distances = np.random.uniform(size=(10000,10000))



class BranchNetwork(object):
    def __init__(self,demos,branch_traits,media,distance_matrix,coeff_ft,coeff_rc,coeff_wt,competitors):
        self.demos = demos.copy()
        self.branch_traits = branch_traits.copy()
        self.media = media.copy()
        self.distance_matrix = distance_matrix.copy()
        self.coeff_ft = coeff_ft.copy()
        self.coeff_rc = coeff_rc.copy()
        self.coeff_wt = coeff_wt.copy()
        self.competitors = competitors.copy()
    
    ############# VARIABLE TRANSFORMS ###################
    # CREATE CUSTOMER-LEVEL VARIABLES
    
    # Net Wealth - some multiple of income
    def calc_net_wealth(self,income):
        result = 2e-6*income**2 + 53.697*income - 2e-6
        return result
    
    # Tech Savvy
    def calc_tech_sav(self,age):
        result = 0.00009*age**3 - 0.016*age**2 + 0.761*age - 1.478 
        return result

    # Segment (definition may change later)
    def derive_segments(self,age,income):
        if age > 60 and income > 100000: 
            result = 'segment1'
        elif age > 60 and income < 30000:
            result = 'segment2'
        elif 30 < age < 60 and income > 120000:
            result = 'segment3'
        elif age < 40 and income<30000:
            result = 'segment4'
        else:
            result = 'segment5'
        return result

    #Community Score
    def calc_community_score(self,age):
        result = -0.0025*age**2 + 0.3017*age +0.0958
        return result


    ## GO FROM CUSTOMER-LEVEL TO BRANCH LEVEL
   
    def agg_demos_to_branch(self,demos):
        result = demos.groupby('branch_id', as_index=False).agg({'age': np.mean,
                                                                 'income_annual':np.mean,
                                                                 'gender':np.mean,
                                                                 'marital_status':np.mean,
                                                                 'net_wealth_assets_liabilities':np.mean,
                                                                 'number_of_dependents':np.mean,
                                                                 'industry_agriculture': np.sum,
                                                                 'industry_mining': np.sum,
                                                                 'industry_manufacturing': np.sum,
                                                                 'industry_electricity_gas_water_and_waste_services': np.sum,
                                                                 'industry_construction': np.sum,
                                                                 'industry_wholesale_trade': np.sum,
                                                                 'industry_retail_trade': np.sum,
                                                                 'industry_accommodation_and_food_services': np.sum,
                                                                 'industry_transport_postal_and_warehousing': np.sum,
                                                                 'industry_information_media_and_telecommunications': np.sum,
                                                                 'industry_rental_hiring_and_real_estate_services': np.sum,
                                                                 'industry_professional_scientific_and_technical_services': np.sum,
                                                                 'industry_administrative_and_support_services': np.sum,
                                                                 'industry_public_administration_and_safety': np.sum,
                                                                 'industry_education_and_training_private': np.sum,
                                                                 'industry_health_care_and_social_assistance': np.sum,
                                                                 'industry_arts_and_recreation_services': np.sum,
                                                                 'industry_other': np.sum,
                                                                 'highest_education_attained_apprenticeship_trade': np.sum,
                                                                 'highest_education_attained_primary_school': np.sum,
                                                                 'highest_education_attained_high_school': np.sum,
                                                                 'highest_education_attained_university_ugrad': np.sum,
                                                                 'highest_education_attained_university_postgrad': np.sum,
                                                                 'community_score':np.mean,
                                                                 'ethnicity_australian':np.sum,
                                                                 'ethnicity_indigenous':np.sum,
                                                                 'ethnicity_asian':np.sum,
                                                                 'ethnicity_islander':np.sum,
                                                                 'employment_full_time': np.sum,
                                                                 'employment_part_time': np.sum,
                                                                 'employment_retired': np.sum,
                                                                 'employment_unemployed': np.sum,
                                                                 'segment1':np.sum,
                                                                 'segment2':np.sum,
                                                                 'segment3':np.sum,
                                                                 'segment4':np.sum,
                                                                 'segment5':np.sum})
        result['age2'] = result['age']**2
        return result

    def transform_demos(self,demos):
        demos['net_wealth_assets_liabilities'] = demos['income_annual'].apply(self.calc_net_wealth)
        demos['tech_savvy_rating'] = demos['age'].apply(self.calc_tech_sav)
        demos['segment'] = demos.apply(lambda x: self.derive_segments(x['age'],x['income_annual']),axis=1)
        demos['community_score'] = demos['age'].apply(self.calc_community_score)
        seg_one_hot= pd.get_dummies(demos['segment'])
        seg_one_hot['customer'] = demos['customer']
        merged_demos = pd.merge(demos.reset_index(),seg_one_hot.reset_index())
        demos_branch = self.agg_demos_to_branch(merged_demos)
        return demos_branch


    ## DERIVED BRANCH-LEVEL VARIABLES
    def calc_branch_NPS(self,wt_avg_ov):
        result = np.round(0.0204*wt_avg_ov**2 - 3.6022*wt_avg_ov + 52.935)
        return result
    
    def calc_median_driving_time_to_competitor(self):
        median_distance = np.median(self.distance_matrix[self.competitors['location_id'].values])
        self.branch_traits['median_driving_time_to_a_competitor_mins'] = median_distance

    
    def median_drive_timedist(self,location_id):
        result = np.median(self.distance_matrix[location_id])
        return result


    def transform_branch_traits(self,branch_traits,coeff_wt):
        # Branch - Average Overall Wait Time
        svc_wait_times_cols = ['avg_wait_time_banking',
                               'avg_wait_time_service_loans',
                               'avg_wait_time_loans', 
                               'avg_wait_time_insurance',
                               'avg_wait_time_fx',
                               'avg_wait_time_bills',
                               'avg_wait_time_investments']
        svc_wait_times = branch_traits[svc_wait_times_cols]

        client_facing_cols = ['numstaff_customer_facing_service',
                              'numstaff_customer_facing_sales']
        client_facing = branch_traits[client_facing_cols]
        client_facing['tot_client_facing'] = (client_facing['numstaff_customer_facing_service'] + 
                                              client_facing['numstaff_customer_facing_sales'])

        wt_variable_vals = pd.concat([svc_wait_times,client_facing['tot_client_facing']],axis=1)

        
        wt_avg_ov = np.matmul(wt_variable_vals.values,np.transpose(coeff_wt.values))
        wt_age_ov_sig = 1/(1-np.exp(4-wt_avg_ov/5))
        # Branch - Sigmoid transform Average Overall Wait Time
        branch_traits['branch_avg_wait_time_sigmoid_transformed'] = wt_age_ov_sig
        # Branch - NPS variable based on Average Overall Wait Time
        branch_traits['branch_nps'] = np.apply_along_axis(self.calc_branch_NPS,0,wt_avg_ov)
        branch_traits['median_distance_to_branch'] = branch_traits['location_id'].apply(self.median_drive_timedist)
                
        return branch_traits


    ################################### MODEL ROUTINES ###################
    def merge_demos_branch_traits(self,transformed_demos,transformed_branch_traits,media):
        merge_demos_branch = pd.merge(transformed_demos,
                                      transformed_branch_traits.reset_index(), 
                                      on='branch_id')
        merge_demos_branch_media = pd.merge(merge_demos_branch.reset_index(),
                                            media.reset_index(), 
                                            on='branch_id')
        merge_demos_branch_media['intercept']=1
        return merge_demos_branch_media

    def pandas_matmul(self,list_of_dfs_for_merging,coeffs_df):
        if len(list_of_dfs_for_merging) == 2:
            model_vals = pd.merge(list_of_dfs_for_merging[0].reset_index(),list_of_dfs_for_merging[1], on='branch_id')
        else:
            model_vals = list_of_dfs_for_merging[0] 

        col_order = [col for col in coeffs_df['coeff'].tolist() if col in model_vals.columns.tolist()]
        model_vals_reordered = model_vals[col_order]
        model_result = np.matmul(model_vals_reordered.values,coeffs_df.set_index('coeff').values)
        model_result_df = pd.DataFrame(model_result,columns=coeffs_df.set_index('coeff').columns.tolist())
        model_result_df = pd.concat([model_vals['branch_id'],model_result_df],axis=1)
        return model_result_df
    
    def rc_breakdown(self, list_of_dfs_for_merging, coeffs_df):
        if len(list_of_dfs_for_merging) == 2:
            model_vals = pd.merge(list_of_dfs_for_merging[0].reset_index(),list_of_dfs_for_merging[1], on='branch_id')
        else:
            model_vals = list_of_dfs_for_merging[0] 

        col_order = [col for col in coeffs_df['coeff'].tolist() if col in model_vals.columns.tolist()]
        model_vals_reordered = model_vals[col_order]
        coeffs_reordered = coeffs_df.set_index('coeff').loc[col_order,:]
        results = []
        for i in range(len(coeffs_reordered.columns.tolist())):
            rc_type = coeffs_reordered.columns.tolist()[i]
            result_matrix = np.multiply(model_vals_reordered.values,coeffs_reordered.values[:, i])
            result_df = pd.DataFrame(result_matrix, columns=map(lambda x: rc_type + "_" + x, model_vals.columns.tolist()[1:]))
            results.append(result_df)
        model_result_df = pd.concat(results, axis=1)
        model_result_df = pd.concat([model_vals['branch_id'],model_result_df],axis=1)
        return model_result_df
    
    
    #### FUNCTIONS FOR ENDPOINTS IN FLASK ####
    # --- call - table (2 dim)
    def branch_performance(self):
        # Build transformed dataset for modelling
        self.calc_median_driving_time_to_competitor() 
        transformed_demos = self.transform_demos(self.demos)
        transformed_branch_traits = self.transform_branch_traits(self.branch_traits, self.coeff_wt)
        merged_data = self.merge_demos_branch_traits(transformed_demos,
                                                     transformed_branch_traits[self.branch_traits.status == 'Open'],
                                                     self.media)
        
        # Calculate Model
         #import pdb; pdb.set_trace()
        ft_result = self.pandas_matmul([merged_data],self.coeff_ft).round()
        ft_breakdown = self.rc_breakdown([ft_result],self.coeff_rc)
        rc_result = self.pandas_matmul([ft_result,merged_data],self.coeff_rc)
        rc_result['profit']=rc_result['revenue']-rc_result['cost']
        interim_result = pd.merge(merged_data,rc_result,on='branch_id')
        interim_result = pd.merge(interim_result,ft_result,on='branch_id')
        final_result = pd.merge(interim_result, ft_breakdown, on='branch_id')
        

        return final_result.set_index('branch_id').to_json()

    # --- call
    def lga_performance(self):
        # Build transformed dataset for modelling
        self.calc_median_driving_time_to_competitor() 
        transformed_demos = self.transform_demos(self.demos)
        transformed_branch_traits = self.transform_branch_traits(self.branch_traits, self.coeff_wt)
        merged_data = self.merge_demos_branch_traits(transformed_demos,
                                                     transformed_branch_traits[self.branch_traits.status == 'Open'],
                                                     self.media)
        print (merged_data)
        # Calculate Model
        ft_result = self.pandas_matmul([merged_data],self.coeff_ft).round()
        ft_breakdown = self.rc_breakdown([ft_result],self.coeff_rc)
        rc_result = self.pandas_matmul([ft_result,merged_data],self.coeff_rc)
        rc_result['profit']=rc_result['revenue']-rc_result['cost']
        interim_result = pd.merge(merged_data,rc_result,on='branch_id')
        interim_result = pd.merge(interim_result,ft_result,on='branch_id')
        final_result = pd.merge(interim_result, ft_breakdown, on='branch_id')
        return final_result.sum().to_json()
    
    def report_demos_pct(self,chart,segment):
        demos_to_report = self.transform_demos(self.demos)
        demos_to_aggregate = ['industry', 'education', 'employment', 'ethnicity', 'segment']
        output_dict = {}
        for demos in demos_to_aggregate:
            if(demos==chart):
                columns_to_aggregate = [col for col in demos_to_report.columns.tolist() if re.search(demos, col) is not None]
                df = demos_to_report[columns_to_aggregate]
                normed_matrix = normalize(df.values, norm='l1', axis=1)
                data= pd.DataFrame(normed_matrix,
                                                  index=self.branch_traits.index.tolist(),
                                                  columns=columns_to_aggregate).to_dict()
                return data[segment]
                
        
        #return json.dumps(output_dict)
    
    def branch_level_demos(self):
        return self.transform_demos(self.demos).set_index('branch_id').to_dict()
    
    def undo_one_hot(self, row):
        for column in self.branch_traits.columns:
            if row[column] == 1:
                return column
    
    def branch_facilities_list(self):
        list_to_return = ['atm_with_deposit_facility','atm_without_deposit_facility', 'atm_with_disabled_access',
                          'mobile_banker', 'quick_change_machine', 'internet_banking_kiosk', 'express_business_deposit',
                          'atm_with_audio_access', 'financial_planner', 'coin_swap_machine', 'small_business_banker',
                          'extended_store_hours', 'store_size_sqm', 'tcr_tele_cash_recycler_vs_teller_counter_and_fly_up_screen',
                         'i_pad_tablets_for_customer_use', 'digital_information_screens', 'queuing_systems',
                          'leisure_zone_coffee_couches_kids_play_zone', 'banking_credit_cards']
        branch_traits_copy = self.branch_traits.copy()
        branch_traits_copy = branch_traits_copy[list_to_return]
        result_dict = defaultdict(lambda: [])
        for branch_id, series in branch_traits_copy.iterrows():
            for index, trait in enumerate(series):
                if trait == 1:
                    result_dict[branch_id].append(list_to_return[index])
        return json.dumps(result_dict)
    
    def branch_products_list(self):
        list_to_return = ['banking_transaction_accounts', 'banking_savings_accounts', 'banking_term_deposits',
                          'banking_internet_banking', 'loans_personal_loans', 'loans_home_loans', 'investments_superannuation',
                          'investments_managed_funds', 'investments_nab_trade', 'fx', 'bills', 'insurance']
        branch_traits_copy = self.branch_traits.copy()
        branch_traits_copy = branch_traits_copy[list_to_return]
        result_dict = defaultdict(lambda: [])
        for branch_id, series in branch_traits_copy.iterrows():
            for index, trait in enumerate(series):
                if trait == 1:
                    result_dict[branch_id].append(list_to_return[index])
        return json.dumps(result_dict)
    

    def change_branch_traits(self,branch_traits_dict):
        for branch_id, trait_mix in branch_traits_dict.items():
            for service, indicator in trait_mix.items():
                self.branch_traits.loc[branch_id, service] = indicator
    
        
    def close_branches(self,branch_dict):  #Call this before optimising #Dictionary mapping BSBs to 'Close' values.
           #import pdb; pdb.set_trace()
            new_demos = self.demos.copy()
            new_branch_traits = self.branch_traits.copy()
            closed_branches = [branch_id for branch_id, status in branch_dict.items() if status == 'Close']
            new_demos.set_index('original_branch', inplace=True)
            for branch_id, open_close in branch_dict.items():
                if open_close == 'Close':
                    new_demos['branch_id'] = new_demos.apply(lambda person: self.churn_result(closed_branches, 
                                                                                              person['branch_id'],
                                                                                              person['location_id'],
                                                                                              person['churn_flag']),axis=1)
                elif open_close == 'Open':
                    new_demos.ix[branch_id, 'branch_id'] = branch_id
                    new_branch_traits.loc[branch_id,'status'] = open_close
            self.branch_traits = new_branch_traits
            self.demos = new_demos.reset_index()
    
  
    def branch_closure_effect(self):

        result = pd.DataFrame(self.demos.original_branch.value_counts())
        result.reset_index(inplace=True)
        before=result.values.tolist()
        #before.insert(0,'before')
        after = pd.DataFrame(self.demos.branch_id.value_counts())
        after.reset_index(inplace=True)
        after=after.values.tolist()
        #after.insert(0,'after')
        
        keys=[]
        for a in after:
            keys.append(a[0])
        
        keys.insert(0,'BSB')
        
        before_vals=[]
        
        for b in before:
            before_vals.append(b[1])
            
        before_vals.insert(0,'Before')
            
        after_vals=[]
        
        for a in after:
            after_vals.append(a[1])
            
        after_vals.insert(0,'After')
            
        return json.dumps([keys,before_vals,after_vals])
        
    ##ignore
    def churn_result(self, closed_branches, current_branch, location_id,churn_flag):
        if current_branch in closed_branches:
            if churn_flag == 1:
                remaining_options = pd.concat([self.branch_traits.reset_index()[['branch_id','location_id']],
                                               self.competitors.reset_index()[['branch_id','location_id']]])
     
                remaining_options.set_index('branch_id',inplace = True)
                remaining_options.drop(closed_branches,inplace = True)
                remaining_options.reset_index(inplace = True)
                option_distances = self.distance_matrix[location_id,
                                                        remaining_options['location_id'].values]
                closest_option = remaining_options['branch_id'].values[np.argmin(option_distances)]
                return closest_option
            else:
                return "Online"
        else:
            return current_branch    
        
branchModel = BranchNetwork(demos, branch_traits, media, location_distances, coeff_ft, coeff_rc, coeff_wt, competitors)