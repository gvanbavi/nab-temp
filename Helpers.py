#############################
#
#	Helpers
#
from decimal import Decimal

def runUpdate(db,stmt,params):
	cursor=db.cursor()
	
	if(params):
		cursor.execute(stmt,params)
	else:
		cursor.execute(stmt)
		
	db.commit()
	
def runMapFetch(db,stmt,params,map):
	data=runFetch(db,stmt,params)
	result=[]
	for d in data:
		element={}
		for i in range(0,len(map)):
			element[map[i]]=d[i]
			
		result.append(element)
	
	return result
	
	
def runFetch(db,stmt,params):
	cursor=db.cursor()

	if(params):
		cursor.execute(stmt,params)
	else:
		cursor.execute(stmt)
		
	return cursor.fetchall()
	
def runBulkInsert(db,stmt,params):
	cursor=db.cursor()
	cursor.executemany(stmt,params)
	db.commit()	
	
def runInsert(db,stmt,params):
	cursor=db.cursor()
	cursor.execute(stmt,params)
	db.commit()
	
def transposeArr(arrList):
	arrs=[]
	for a in arrList:
		temp=[]
		for e in a:
			temp.append(e)
		arrs.append(temp)
		
	return arrs
	
	
def arrToColumns(arrList,cols):
	"""
		[ ['col1value', 'col2value'],  ]
	"""
	arrs=[]
	for i in range(0,len(arrList[0])):
		arrs.append([cols[i]])


	for a in arrList:
		for i in range(0,len(arrList[0])):
			value = a[i]
			if type(value) == Decimal:
				value = float(value)
			arrs[i].append(value)
			
	return arrs

def branch_facilities(data1,dadict,selection):

    if selection=='branch':

        # branch
        branch_keys =   {'branch_id':'Branch Id',
                         'location': 'Location type',
                         'extended_store_hours':'Has extended hours',
                         #'store_size_sqm':'Store bigger than 200 sqm?',
                         'max_driving_time_to_a_competitor_mins':'Driving time to next NAB branch [min]',
                         #'max_driving_time_to_an_atm':'Max driving to an ATM',
                         #'leisure_zone_coffee_couches_kids_play_zone':'Leisure zone present?',
                         'car_park_proximity':'Distance to car park [min]'
                         #'mobile_banker':'Mobile banker on premise ?',
                         #'internet_banking_kiosk':'Has internet bank kiosk ?',
                         #'financial_planner':'Financial planner on premise ?',
                         #'small_business_banker':'Small business banker ?',
                         #'quick_change_machine':'Quick change machine ?',
                         #'express_business_deposit':'Express business deposit ?',
                         #'coin_swap_machine': 'Coin swap machine',
                         #'atm_with_disabled_access':'Disabled access at ATM ?',
                         #'atm_with_deposit_facility':'Deposit facility at ATM ?',
                         #'atm_with_audio_access':'Audio access with ATM',
                         #'queuing_systems':'Queuing Systems',
                         #'negative_social_media_sentiment_in_last_week':'Social Media sentiment'}
                         }

        branch_names=['branch_id','location','extended_store_hours',
                      #'store_size_sqm',
                      'max_driving_time_to_a_competitor_mins',
                      #'leisure_zone_coffee_couches_kids_play_zone',
                      'car_park_proximity']
                      #'mobile_banker','internet_banking_kiosk','financial_planner','small_business_banker','quick_change_machine','express_business_deposit','coin_swap_machine','atm_with_disabled_access',
                      #'atm_with_deposit_facility','atm_with_audio_access','queuing_systems','negative_social_media_sentiment_in_last_week']

        data=[]
        for key in branch_names:


            if key=='location':

                keys2   =   ['branch_location_high_street', 'branch_location_free_standing','branch_location_mall']
                location=   [key for key in keys2 if dadict[key]==1]

                if len(location)==0:

                    sel   =     'Spoke'

                elif 'mall':

                    sel   =     'Spoke'

                elif 'free':

                    sel    =    'Spoke'

                else:

                    sel    =    'Spoke'

                data.append([branch_keys[key],sel])

            elif key=='branch_id':

                data.append([branch_keys[key],'Eastwood'])

            elif (key=='max_driving_time_to_a_competitor_mins') or (key=='max_driving_time_to_an_atm') or (key=='car_park_proximity'):

                val =   dadict[key]/10.0
                data.append([branch_keys[key],val])


            elif key=='negative_social_media_sentiment_in_last_week':

                if dadict[key]==0:

                    val='Positive'

                else:

                    val='Negative'

                data.append([branch_keys[key],val])

            else:

                if dadict[key]==1:

                    val =   'Yes'

                elif dadict[key]==0:

                    val =   'No'

                else:

                    val =   dadict[key]

                data.append([branch_keys[key],val])

        return data

    elif selection=='staff':

        # staff
        staff_keys =   {'numstaff_staff_help':'Staff Help','numstaff_customer_facing_service': 'CF Service', 'numstaff_customer_facing_sales':'Sales', 'numstaff_non_customer_facing_service':'Non-CF Service','numstaff_non_customer_facing_sales_admin':'Sales Admin', 'numstaff_non_customer_facing_general_admin':'General Admin','numstaff_management_activities':'Management','numstaff_training':'Training'}

        staff_names = ['numstaff_staff_help','numstaff_customer_facing_service','numstaff_customer_facing_sales','numstaff_non_customer_facing_service','numstaff_non_customer_facing_sales_admin','numstaff_non_customer_facing_general_admin','numstaff_management_activities','numstaff_training']



        data=[]
        for key in staff_names:

            val =   dadict[key]
            data.append([staff_keys[key],val])

        return data

    elif selection=='skills':

        # skills
        skills_keys=['bills','banking','fx','insurance','investments','loans','staff_average_age','staff_remuneration','staff_language_skills_num_langauges']
        skill_name={'bills':'Bills','banking':'Banking','fx':'Foreign Exchange','insurance':'Insurance','investments':'Investments','loans':'Loans','staff_average_age':'Average staff age','staff_remuneration':'Avgerage staff remuneration','staff_language_skills_num_langauges': 'No. langauage spoken by staff'}

        data=[]
        for key in skills_keys:

            if key=='banking':

                tempkeys={'banking_savings_accounts':'SA','banking_term_deposits':'TD','banking_credit_cards':'CC','banking_transaction_accounts':'TA'}

                val=[tempkeys[key] for key in tempkeys if dadict[key]==1]

                data.append([skill_name[key],', '.join(val)])

            elif key=='investments':

                tempkeys={'investments_nab_trade':'Tr','investments_managed_funds':'MF','investments_superannuation':'SA'}

                val=[tempkeys[key] for key in tempkeys if dadict[key]==1]

                data.append([skill_name[key],', '.join(val)])

            elif key=='loans':

                tempkeys={'loans_personal_loans':'PL','loans_home_loans':'HL'}

                val=[tempkeys[key] for key in tempkeys if dadict[key]==1]

                data.append([skill_name[key],', '.join(val)])

            elif key=='staff_language_skills_num_langauges':

                data.append([skill_name[key],dadict[key]])

            else:

                if dadict[key]==1:

                    val =   'Yes'

                elif dadict[key]==0:

                    val =   'No'

                else:

                    val =   dadict[key]

                data.append([skill_name[key],val])

        return data

    elif selection=='waiting':

        # waiting times
        wait_keys=	['avg_wait_time_fx',
                    'avg_wait_time_loans',
                    'avg_wait_time_banking',
                    'avg_wait_time_service_loans',
                    'avg_wait_time_insurance',
                    'avg_wait_time_investments',
                    'avg_wait_time_bills'
                    ]

        data=[]
        data.append(['Average waiting time [min]',round(sum([dadict[key] for key in wait_keys])/len(wait_keys),1)])

        '''
        for key in wait_keys:

            if dadict[key]==1:

                val =   'Yes'

            elif dadict[key]==0:

                val =   'No'

            else:

                val =   dadict[key]

            if key=='queuing_systems':

                data.append([key.replace('_',' '),val])

            else:

                data.append([key.replace('_',' ')+' [min]',val])
        '''

        return data

