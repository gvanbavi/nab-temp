from flask import Flask, render_template, request, send_from_directory
import json
import Globals
import Helpers

app = Flask(__name__,static_url_path='')

@app.route("/kml",methods=['GET'])
def generateKML():
	lga=request.args.get('lga')
	boundaries=Helpers.runFetch(Globals.db,"""SELECT boundary FROM lga_boundaries WHERE LGA=%(LGA)s""",{"LGA":lga})
	boundary_str=boundaries[0][0]
	return Globals.kml_header+boundary_str+Globals.kml_footer
	
if __name__ == "__main__":
	app.run()