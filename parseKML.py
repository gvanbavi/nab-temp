import Globals
import re 

with open('lga_boundaries.kml', 'r') as myfile:
    data=myfile.readlines()
	
place=''
state_code=''
lga_code=''
lga_name=''

def insertPlace(place,state,lga,name):
	cursor=Globals.db.cursor()
	
	cursor.execute('INSERT INTO lga_boundaries(boundary,state,LGA,name) VALUES(%(boundary)s,%(state)s,%(LGA)s,%(name)s)',{'boundary':place,'state':state,'LGA':lga,'name':name})
	
	Globals.db.commit()

for d in data:
	if '/Placemark' in d:
		place=place+d
		insertPlace(place,state_code,lga_code,lga_name)
		place=''
		state_code=''
		lga_code=''
		lga_name=''
	else:
		if 'STATE_CODE' in d:
			x=re.search('([\d]+)',d)
			state_code=x.group(0)
		if 'LGA_CODE11' in d:
			x=re.search('([\d]{3,10})',d)
			lga_code=x.group(0)
		if 'LGA_NAME11' in d:
			x=re.search('>([^>]+)<',d)
			lga_name=x.group(1)
		place=place+d

