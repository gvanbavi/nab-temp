from flask import Flask, render_template, request, send_from_directory
import json
import Globals
import Helpers
import random
import operator
import numpy as np
import array
import BranchOptimisation as BO

# visit: http://192.168.0.210:5000/overview

app = Flask(__name__,static_url_path='')
app.config["CACHE_TYPE"] = "null"
app.config['TEMPLATES_AUTO_RELOAD'] = True

################################## BOILER PLATE #######################################

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

	
@app.route('/js/<path:path>')
def send_js(path):
	return send_from_directory('js', path)
	
@app.route('/css/<path:path>')
def send_css(path):
	return send_from_directory('css', path)
	
@app.route('/fonts/<path:path>')
def send_fonts(path):
	return send_from_directory('fonts', path)
	
@app.route('/img/<path:path>')
def send_imgs(path):
	return send_from_directory('img', path)
	
@app.route('/vendors/<path:path>')
def send_vendors(path):
	return send_from_directory('vendors', path)
	

	
################################## ROUTES #######################################

@app.route("/")
def hello():

	dict={'a':1234,'b':4567}

	return render_template("index.html",result=dict,run="run")
	
@app.route("/overview")
def overview():
	return render_template("overview.html",overview="active")
	
@app.route("/lens1")
def lensOneView():
	return render_template("lens1.html",lens1="active")
	
@app.route("/lens2")
def lensTwoView():
	return render_template("lens2.html",lens2="active")
	
@app.route("/lens3")
def lensThreeView():
	return render_template("lens3.html",lens3="active")
	
@app.route("/lens4")
def lensFourView():
	return render_template("lens4.html",lens4="active")
	
@app.route("/route",methods=['POST'])
def getRoute():
	print(request.data)
	data = json.loads(request.data)
	return data["start"]

@app.route("/kml",methods=['GET'])
def generateKML():
	lga=request.args.get('lga')
	boundaries=Helpers.runFetch(Globals.db,"""SELECT boundary FROM lga_boundaries WHERE LGA=%(LGA)s""",{"LGA":lga})
	boundary_str=boundaries[0][0]
	return Globals.kml_header+boundary_str+Globals.kml_footer
	
@app.route("/branch/performance",methods=['GET'])
def getBranchPerformance():
	return BO.branchModel.branch_performance()
	
@app.route("/lga/performance",methods=['GET'])
def getLGAPerformance():
	return BO.branchModel.lga_performance()
	

	
@app.route("/branch/close",methods=['POST'])
def closeBranch():
	print("=============================== here")
	print(request.get_json())
	branches=json.loads(request.data)
	BO.branchModel.close_branches(branches)
	data=BO.branchModel.branch_closure_effect()

	print ('-----' +data)

	return data
	
def dictToList(dict):
	list=[]
	for k in dict:
		list.append([k,dict[k]])
		
	return list
	
@app.route("/report/demo",methods=['GET'])
def reportDemo():
	chart=request.args.get('chart')
	segment=request.args.get('segment')

	data = BO.branchModel.report_demos_pct(chart,segment)
	data=dictToList(data)
	return json.dumps({'data':data})
	

################################## CHARTS & TABLES #######################################
# Lens 1 (table 1)
@app.route('/LGAs')
def getLGAs():
	query = """
		select 
			a.LGA_name, 
			pop_growth,
			cnt/per100k 
		from
			(select LGA_name,count(*) as cnt from branch_geo where bank = 'NAB' group by LGA_name) as a
		left join 
			(select Lga_name, round(pop_growth,3) as pop_growth,pop_2011/100000 as per100k FROM nab.popgrowth_lga) as b
		on a.LGA_name = b.LGA_name
		order by LGA_name;
	"""
	data=Helpers.runFetch(Globals.db,query,{})
	data=Helpers.arrToColumns(data,['LGA','Pop_growth','bankPerLGA',])
	return json.dumps(data)


# Lens 1 map (chart 2)
@app.route('/branches/locations')
def getBranchLocations():
	query='SELECT bsb, bank, description, CONCAT(`long`),CONCAT(lat) FROM branches_sydney LIMIT 20';
	args={}
	clause=''
	query_data=request.args.get('banks')
	
	if(query_data):
		query_data=query_data.split(',')
		clause=" bank='"+" OR bank='".join(query_data)+"'"
		query="SELECT branch_id, bank, bank, CONCAT(`longitude`),CONCAT(`latitude`) FROM nab.branch_geolocation where suburb='Parramatta' and "+clause+";"

	print (query)
		
	data=Helpers.runMapFetch(Globals.db,query,{},['bsb','bank','description','long','lat'])
		
	return json.dumps(data)

# Lens 1 industry growth (chart 3)
@app.route('/industry/growth')
def getIndustryGrowth():
	query = """
		select 
			sum(Agriculture_forestry_fishing) as Agriculture_forestry_fishing,
			sum(Mining) as Mining,sum(Manufacturing) as Manufacturing,
			sum(Electricity_Gas_Water_Waste_servives) as Electricity_Gas_Water_Waste_servives,
			sum(Construction) as Construction,
			sum(Wholesale_Trade) as Wholesale_Trade,
			sum(Retail_Trade) as Retail_Trade,
			sum(Accomodation_food_services) as Accomodation_food_services,
			sum(Transport_postal_warehousing) as Transport_postal_warehousing,
			sum(Information_media_and_telecommunications) as Information_media_and_telecommunications,
			sum(Financial_and_insurance_services) as Financial_and_insurance_services,
			sum(Real_estate_services) as Real_estate_services,
			sum(Professional_scientific_and_techinical_services) as Professional_scientific_and_techinical_services,
			sum(Admin_and_support_services) as Admin_and_support_services,
			sum(Public_admin_and_safety) as Public_admin_and_safety,
			sum(Education_and_training) as Education_and_training,
			sum(Healthcare_and_social_assistance) as Healthcare_and_social_assistance,
			sum(Arts_and_recreation_services) as Arts_and_recreation_services,
			sum(Other_services) as Other_services 
		from 
			nab.industry_of_employment_by_lga
	"""
	population = Helpers.runFetch(Globals.db,query,{})[0]
	
	x_data = [
		'Agriculture_forestry_fishing',
		'Mining',
		'Manufacturing',
		'Electricity_Gas_Water_Waste_servives',
		'Construction',
		'Wholesale_Trade',
		'Retail_Trade',
		'Accomodation_food_services',
		'Transport_postal_warehousing',
		'Information_media_and_telecommunications',
		'Financial_and_insurance_services',
		'Real_estate_services',
		'Professional_scientific_and_techinical_services',
		'Admin_and_support_services',
		'Public_admin_and_safety',
		'Education_and_training',
		'Healthcare_and_social_assistance',
		'Arts_and_recreation_services',
		'Other_services'
	]
	
	vectors = [
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227,
		1.07227
	]

	# the x axis should be industry name
	return json.dumps({'data': [
		['x'] + x_data,
		['population (current)'] + [float(value) for value in population],
		['population (future)'] + [round(float(value) * float(vectors[index])) for index, value in enumerate(population)],
		#['population (future)'] + [round(float(value) * 1.07227,0) for value in population]
	]})
	

# Lens 1 age distribution (chart 4)
@app.route('/age/distribution')
def getAgeDistribution():
	query = """
		select age,sum(pop) as current,cast(sum(pop) * 1.05 as unsigned) as future 
			from pop_lga 
			where age not in ('0-4','5-9') 
			group by age
		union
		select age, sum(pop) as current,cast(sum(pop) * 1.05 as unsigned) as future 
			from (
					select '0-9' as age,sum(pop) as pop from pop_lga where age in ('0-4','5-9') group by age) as b 
			group by age
		order by age
	"""
	data=Helpers.runFetch(Globals.db,query,{})

	data=Helpers.arrToColumns(data,['x','Age (current)','Age (future)'])
	return json.dumps({'data': data})

# Lens 1 growth of origin of nationalities (chart 5)
@app.route('/origin/growth')
def getOriginGrowth():
	query = """
		SELECT sum(United_Kingdom) as United_Kingdom,
			sum(Born_elsewhere) as Born_elsewhere,
			sum(New_Zealand) as New_Zealand,
			sum(China) as China,
			sum(India) as India,
			sum(Italy) as Italy,
			sum(Vietnam) as Vietnam,
			sum(Phillippines) as Phillippines,
			sum(South_Africa) as South_Africa,
			sum(Malaysia) as Malaysia,
			sum(Germany) as Germany,
			sum(Greece) as Greece,
			sum(Sri_Lanka) as Sri_Lanka,
			sum(United_states) as United_states,
			sum(Lebanon) as Lebanon
		FROM country_of_birth_lga

	"""
	population = Helpers.runFetch(Globals.db,query,{})[0]
	
	x_data = [
		'United_Kingdom',
		'Born_elsewhere',
		'New_Zealand',
		'China',
		'India',
		'Italy',
		'Vietnam',
		'Phillippines',
		'South_Africa',
		'Malaysia',
		'Germany',
		'Greece',
		'Sri_Lanka',
		'United_states',
		'Lebanon'
		]
	
	return json.dumps({'data': [
		['x'] + x_data,
		['population (current)'] + [float(value) for value in population],
		#['population (future)'] + [round(float(value) * float(vectors[index])) for index, value in enumerate(population)],
		['population (future)'] + [round(float(value) * 1.04, 0) for value in population]
	]})

# Lens 2 (chart 1)
@app.route('/parramatta/revenue')
def getLGARevenue():
	query_optimised = None
	chosen = request.args.get('chosen', None)
	factorClosed = not request.args.get('factorClosed', '') == ''

	x_data = [
			'banking',
			'bills',
			'fx',
			'insurance',
			'investments',
			'loan',
			'service loans'
		]

	if factorClosed == False:
		if chosen == 'location':
			query_optimised = """
				select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
				from currentstate_branchperf_optimised_drivetime_parramatta
			"""
		if chosen == 'product':
			query_optimised = """
				select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
				from currentstate_branchperf_optimised_prodmix_parramatta
			"""
		if chosen == 'branch':
			query_optimised = """
				select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
				from currentstate_branchperf_optimised_facmix_parramatta
			"""
	
		query_current = """
			select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
			from currentstate_branchperformance_parramatta
		"""
		
		query_future = """
			select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
			from futurestate_branchperformance_parramatta
		"""
		
		current=Helpers.runFetch(Globals.db,query_current,{})
		future=Helpers.runFetch(Globals.db,query_future,{})
		
		if query_optimised is not None:
			optimised=Helpers.runFetch(Globals.db,query_optimised,{})
			return json.dumps({'data': [
				['x'] + x_data,
				['revenue (current)'] + [float(value) for value in current[0]],
				['revenue (future)'] + [float(value) for value in future[0]],
				['revenue (optimised)'] + [float(value) for value in optimised[0]]
			]})
		else:
			return json.dumps({'data': [
				['x'] + x_data,
				['revenue (current)'] + [float(value) for value in current[0]],
				['revenue (future)'] + [float(value) for value in future[0]]
			]})
	else:
		closed_ids = request.args.get('factorClosed')
		closed_ids_list = closed_ids.split(',')
		print("===========================")
		print(closed_ids)
		query_simulation = """
				select SUM(revenue_banking) AS revenue_banking,
					SUM(revenue_bills) AS revenue_bills,
					SUM(revenue_fx) AS revenue_fx,
					SUM(revenue_insurance) AS revenue_insurance,
					SUM(revenue_investments) AS revenue_investments,
					SUM(revenue_loans) AS revenue_loans,
					SUM(revenue_service_loans) AS revenue_service_loans
				from currentstate_branchperf_branchesclosed_parramatta_single
				where branch_id not in (%s)
			""" % (','.join(["'%s'" % closed_id for closed_id in closed_ids]))
		x_data = [x_val for x_val in x_data if x_val not in closed_ids_list]

		simulation=Helpers.runFetch(Globals.db,query_simulation,{})

		print (simulation)

		return json.dumps({'data': [
				['x'] + x_data,
				['revenue (simulation)'] + [float(value) for value in simulation[0]]
			]})

# Lens 2 (chart 2)
@app.route('/parramatta/foottraffic')
def getFootTraffic():
	query_optimised = None
	chosen = request.args.get('chosen', None)
	factorClosed = not request.args.get('factorClosed', '') == ''
	
	x_data = [
		'082-022',
		'082-228',
		'082-301',
		'082-303',
		'082-324',
		'082-330',
		'082-415',
		'082-948',
	]
	
	if factorClosed == False:
		if chosen == 'location':
			query_optimised = """
				select 
					cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic
				from currentstate_branchperf_optimised_drivetime_parramatta
			"""
		if chosen == 'product':
			query_optimised = """
				select 
					cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic	
				from currentstate_branchperf_optimised_prodmix_parramatta		
			"""
		if chosen == 'branch':
			query_optimised = """
				select 
					cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic	
				from currentstate_branchperf_optimised_facmix_parramatta		
			"""
			
			
		query_current = """
			select 
					cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic
			from currentstate_branchperformance_parramatta
	
		"""
		
		query_future = """
			select 
					cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic
			from futurestate_branchperformance_parramatta
	
		"""
		current=Helpers.runFetch(Globals.db,query_current,{})
		future=Helpers.runFetch(Globals.db,query_future,{})
		
		if query_optimised is not None:
			optimised=Helpers.runFetch(Globals.db,query_optimised,{})
			return json.dumps({'data': [
				['x'] + x_data,
				['foot traffic (current)'] + [value for value in current],
				['foot traffic (future)'] + [value for value in future],
				['foot traffic (optimised)'] + [value for value in optimised]
			]})	
		else:
			return json.dumps({'data': [
				['x'] + x_data,
				['foot traffic (current)'] + [value for value in current],
				['foot traffic (future)'] + [value for value in future]
			]})
	else:
		closed_ids = request.args.get('factorClosed')
		closed_ids_list = closed_ids.split(',')
		print("===========================")
		print(closed_ids)
		query_simulation = """
				select cast((foot_traffic_banking + 
					foot_traffic_bills +
					foot_traffic_fx +
					foot_traffic_insurance +
					foot_traffic_investments +
					foot_traffic_loans +
					foot_traffic_service_loans)/10 as unsigned) as foot_traffic
				from currentstate_branchperf_branchesclosed_parramatta_single
				where branch_id not in (%s)
			""" % (','.join(["'%s'" % closed_id for closed_id in closed_ids]))
		x_data = [x_val for x_val in x_data if x_val not in closed_ids_list]

		simulation=Helpers.runFetch(Globals.db,query_simulation,{})
		return json.dumps({'data': [
				['x'] + x_data,
				['foot traffic (simulation)'] + [float(value[0]) for value in simulation]
			]})

# Lens 2 (chart 7)
@app.route('/parramatta/branchrevenue')
def getBranchRevenue():
	query_optimised = None
	chosen = request.args.get('chosen', None)
	factorClosed = not request.args.get('factorClosed', '') == ''
	
	x_data = [
		'082-022',
		'082-228',
		'082-301',
		'082-303',
		'082-324',
		'082-330',
		'082-415',
		'082-948',
	]
	
	if factorClosed == False:
		if chosen == 'location':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue
				from currentstate_branchperf_optimised_drivetime_parramatta
			"""
		if chosen == 'product':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue	
				from currentstate_branchperf_optimised_prodmix_parramatta		
			"""
		if chosen == 'branch':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue	
				from currentstate_branchperf_optimised_facmix_parramatta		
			"""
			
		query_current = """
			select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue
			from currentstate_branchperformance_parramatta
	
		"""
		
		query_future = """
			select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue
			from futurestate_branchperformance_parramatta
	
		"""
		current=Helpers.runFetch(Globals.db,query_current,{})
		future=Helpers.runFetch(Globals.db,query_future,{})
		
		if query_optimised is not None:
			optimised=Helpers.runFetch(Globals.db,query_optimised,{})
			return json.dumps({'data': [
				['x'] + x_data,
				['revenue (current)'] + [value for value in current],
				['revenue (future)'] + [value for value in future],
				['revenue (optimised)'] + [value for value in optimised]
			]})	
		else:
			return json.dumps({'data': [
				['x'] + x_data,
				['revenue (current)'] + [value for value in current],
				['revenue (future)'] + [value for value in future]
			]})
	else:
		closed_ids = request.args.get('factorClosed')
		closed_ids_list = closed_ids.split(',')
		print("===========================")
		print(closed_ids)
		query_simulation = """
				select cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans as unsigned) as revenue	
				from currentstate_branchperf_branchesclosed_parramatta_single
				where branch_id not in (%s)
			""" % (','.join(["'%s'" % closed_id for closed_id in closed_ids]))
		x_data = [x_val for x_val in x_data if x_val not in closed_ids_list]

		simulation=Helpers.runFetch(Globals.db,query_simulation,{})
		return json.dumps({'data': [
				['x'] + x_data,
				['revenue (simulation)'] + [value for value in simulation]
			]})

# Lens 2 (chart 8)
@app.route('/parramatta/branchprofit')
def getBranchProfit():
	query_optimised = None
	chosen = request.args.get('chosen', None)
	factorClosed = not request.args.get('factorClosed', '') == ''

	x_data = [
		'082-022',
		'082-228',
		'082-301',
		'082-303',
		'082-324',
		'082-330',
		'082-415',
		'082-948',
	]
	if factorClosed == False:
		if chosen == 'location':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit
				from currentstate_branchperf_optimised_drivetime_parramatta
			"""
		if chosen == 'product':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit	
				from currentstate_branchperf_optimised_prodmix_parramatta		
			"""
		if chosen == 'branch':
			query_optimised = """
				select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit	
				from currentstate_branchperf_optimised_facmix_parramatta		
			"""
		query_current = """
			select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit
			from currentstate_branchperformance_parramatta
	
		"""
		query_future = """
			select 
					cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit
			from futurestate_branchperformance_parramatta
	
		"""
		current=Helpers.runFetch(Globals.db,query_current,{})
		future=Helpers.runFetch(Globals.db,query_future,{})
		
		if query_optimised is not None:
			optimised=Helpers.runFetch(Globals.db,query_optimised,{})
			return json.dumps({'data': [
				['x'] + x_data,
				['profit (current)'] + [value for value in current],
				['profit (future)'] + [value for value in future],
				['profit (optimised)'] + [value for value in optimised]
			]})	
		else:
			return json.dumps({'data': [
				['x'] + x_data,
				['profit (current)'] + [value for value in current],
				['profit (future)'] + [value for value in future]
			]})
	
	else:
		closed_ids = request.args.get('factorClosed')
		closed_ids_list = closed_ids.split(',')
		print("===========================")
		print(closed_ids)
		query_simulation = """
				select cast(revenue_banking + 
					revenue_bills +
					revenue_fx +
					revenue_insurance +
					revenue_investments +
					revenue_loans +
					revenue_service_loans -
					cost_banking -
					cost_bills -
					cost_fx -
					cost_insurance -
					cost_investments -
					cost_loans -
					cost_service_loans
					as unsigned) as profit
				from currentstate_branchperf_branchesclosed_parramatta_single
				where branch_id not in (%s)
			""" % (','.join(["'%s'" % closed_id for closed_id in closed_ids]))
		x_data = [x_val for x_val in x_data if x_val not in closed_ids_list]

		simulation=Helpers.runFetch(Globals.db,query_simulation,{})
		return json.dumps({'data': [
				['x'] + x_data,
				['profit (simulation)'] + [value for value in simulation]
			]})


# Lens 2 (chart 4)
@app.route('/parramatta/agedist')
def getAgeDistParramatta():
	query = """
		select age, cast(sum(pop) as unsigned) as current, cast(sum(pop) * 1.024 as unsigned) as future 
		from pop_lga 
		where LGA_name like 'Parramatta%' and age not in ('0-4','5-9') 
		group by age
		union
			select age, cast(sum(pop) as unsigned) as current, cast(sum(pop) * 1.024 as unsigned) as future 
			from (
					select '0-9' as age, sum(pop) as pop 
					from pop_lga 
					where LGA_name like 'Parramatta%' and age in ('0-4','5-9') 
					group by age
				) as b 
			group by age
		order by age
	"""

	data=Helpers.runFetch(Globals.db,query,{})
	
	data=Helpers.arrToColumns(data,['x','Age (current)','Age (future)'])
	return json.dumps({'data': data})

# Lens 2 (chart 5)	
@app.route('/parramatta/incomedist')
def getIncomeDistParramatta():
	current=Helpers.runFetch(Globals.db,"SELECT CAST(average_income AS UNSIGNED) FROM income_lga where LGA_name like 'Parramatta%'",{})
	x_data = ['average annual income']
	return json.dumps({'data': [
		['x'] + x_data,
		['income (current)'] + [float(current[0][0])],
		['income (future)'] + [float(current[0][0] * 1.022)]
	]})

# Lens 2 (chart 6)
@app.route('/parramatta/industrydist')
def getIndustryDistParramatta():
	query = """
		select sum(industry_agriculture) as industry_agriculture,
				sum(industry_health_care_and_social_assistance) as industry_health_care_and_social_assistance,
				sum(industry_construction) as industry_construction,
				sum(industry_accommodation_and_food_services) as industry_accommodation_and_food_services,
				sum(industry_mining) as industry_mining,
				sum(industry_public_administration_and_safety) as industry_public_administration_and_safety,
				sum(industry_administrative_and_support_services) as industry_administrative_and_support_services,
				sum(industry_wholesale_trade) as industry_wholesale_trade,
				sum(industry_manufacturing) as industry_manufacturing,
				sum(industry_information_media_and_telecommunications) as industry_information_media_and_telecommunications,
				sum(industry_other) as industry_other,
				sum(industry_arts_and_recreation_services) as industry_arts_and_recreation_services,
				sum(industry_education_and_training_private) as industry_education_and_training_private,
				sum(industry_transport_postal_and_warehousing) as industry_transport_postal_and_warehousing,
				sum(industry_professional_scientific_and_technical_services) as industry_professional_scientific_and_technical_services,
				sum(industry_electricity_gas_water_and_waste_services) as industry_electricity_gas_water_and_waste_services,
				sum(industry_retail_trade) as industry_retail_trade,
				sum(industry_rental_hiring_and_real_estate_services) as industry_rental_hiring_and_real_estate_services
		from currentstate_demos_parramatta
	"""
	current=Helpers.runFetch(Globals.db,query,{})
	x_data = [
		'agriculture',
		'health care and social assistance',
		'construction',
		'accommodation and food services',
		'mining',
		'public administration and safety',
		'administrative and support services',
		'wholesale trade',
		'manufacturing',
		'information media and telecommunications',
		'other',
		'arts and recreation services',
		'education and training private',
		'transport postal and warehousing',
		'professional scientific and technical services',
		'electricity gas water and waste services',
		'retail trade',
		'rental hiring and real estate services'
	]
	
	return json.dumps({'data': [
		['x'] + x_data,
		['industry distribution (current)'] + [float(value) for value in current[0]],
		['industry distribution (future)'] + [float(value) * 1.024 for value in current[0]]
	]})


@app.route('/parramatta/summary')
def getsummmarylga():

	output 	=	[]

	# pop
	queryv   =   "SELECT pop_2011*1.024 FROM nab.popgrowth_lga where LGA_name like 'Parramatta%';"
	data1   =   Helpers.runFetch(Globals.db,queryv,{})
	output.append(['Population',round(data1[0][0])])

	# bank presence
	queryv   =   "SELECT count(*) FROM nab.branch_geolocation where suburb='Parramatta';"
	data2   =   Helpers.runFetch(Globals.db,queryv,{})
	output.append(['Bank presence',data2[0][0]])

	# nab presence
	queryv   =   "SELECT count(*) FROM nab.branch_geolocation where suburb='Parramatta' and bank='NAB';	"
	data3   =   Helpers.runFetch(Globals.db,queryv,{})
	output.append(['NAB presence',data3[0][0]])

	# median age, income
	queryv   =   "SELECT median_age,median_income FROM nab.income_lga where Lga_name='Parramatta';"
	data4    =   Helpers.runFetch(Globals.db,queryv,{})
	output.append(['Median age',data4[0][0]])
	output.append(['Median income',data4[0][1]])

	# top 3 industries
	values   =  ['Agriculture_forestry_fishing','Mining','Manufacturing','Electricity_Gas_Water_Waste_servives','Construction','Wholesale_Trade','Retail_Trade','Accomodation_food_services','Transport_postal_warehousing','Information_media_and_telecommunications','Financial_and_insurance_services','Real_estate_services','Professional_scientific_and_techinical_services','Admin_and_support_services','Public_admin_and_safety','Education_and_training','Healthcare_and_social_assistance','Arts_and_recreation_services']
	queryv 	 =	"SELECT Agriculture_forestry_fishing,Mining,Manufacturing,Electricity_Gas_Water_Waste_servives,Construction,Wholesale_Trade,Retail_Trade,Accomodation_food_services,Transport_postal_warehousing,Information_media_and_telecommunications,Financial_and_insurance_services,Real_estate_services,Professional_scientific_and_techinical_services,Admin_and_support_services,Public_admin_and_safety,Education_and_training,Healthcare_and_social_assistance,Arts_and_recreation_services FROM nab.industry_of_employment_by_lga where LGA_name like 'Parramatta%';"
	data5    =   Helpers.runFetch(Globals.db,queryv,{})
	data5    =   dict(zip(values, list(data5[0])))
	data5    =   sorted(data5.items(), key=operator.itemgetter(1))
	data5    =   [x[0] for x in data5[-3:]][::-1]
	output.append(['Top 3 industries',', '.join(data5).replace('_',' ')])


	# top 3 ethnicites
	values   =  ['Australian_Aboriginal','Chinese','Croatian','Dutch','English','Filipino','French','German','Greek','Hungarian','Indian','Irish','Italian','Korean','Lebanese','Macedonian','Maltese','Maori','New_Zealender','Polish','Russian','Scottish','Serbian','Sinhalese','South_African','Spanish','Turkish','Vietnamese','Welsh']
	queryv 	 =	"SELECT Australian_Aboriginal,Chinese,Croatian,Dutch,English,Filipino,French,German,Greek,Hungarian,Indian,Irish,Italian,Korean,Lebanese,Macedonian,Maltese,Maori,New_Zealender,Polish,Russian,Scottish,Serbian,Sinhalese,South_African,Spanish,Turkish,Vietnamese,Welsh FROM nab.birthplace_of_parents_lga where LGA_name like 'Parramatta%';"
	data6    =   Helpers.runFetch(Globals.db,queryv,{})
	data6    =   dict(zip(values, list(data6[0])))
	data6    =   sorted(data6.items(), key=operator.itemgetter(1))
	data6    =   [x[0] for x in data6[-3:]][::-1]
	output.append(['Top 3 ethnicities',', '.join(data6).replace('_',' ')])


	return json.dumps(Helpers.arrToColumns(output,['Parameter','Value']))

@app.route('/parramatta/detail')
def getdetaillga():

	output=[]

	# branch_id, community_score, branch_nps
	queryv   =   "select branch_id,community_score,branch_nps from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	branchid =   [x[0] for x in data1]
	com_sco  =   [int(x) for x in (np.array([float(x[1]) for x in data1])-7)/(8-min(np.array([float(x[1]) for x in data1])))*100]
	branchnps=   [x[2] for x in data1]
	output.append(branchid)
	output.append(com_sco)
	output.append(branchnps)

	# waiting time
	queryv   =   "select (avg_wait_time_banking+avg_wait_time_bills+avg_wait_time_fx+avg_wait_time_insurance+avg_wait_time_investments+avg_wait_time_loans+avg_wait_time_service_loans)/7 from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	waiting  =   [str(round(x[0],1)) for x in data1]
	output.append(waiting)

	# foot traffic
	queryv   =   "select foot_traffic_banking+foot_traffic_bills+foot_traffic_fx+foot_traffic_insurance+foot_traffic_investments+foot_traffic_loans+foot_traffic_service_loans from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	foot     =   [x[0] for x in data1]
	output.append(foot)

	# total staff
	queryv   =   "select numstaff_customer_facing_sales+numstaff_customer_facing_service+numstaff_management_activities+numstaff_non_customer_facing_general_admin+numstaff_non_customer_facing_sales_admin+numstaff_non_customer_facing_service+numstaff_staff_help+numstaff_training from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	staff    =   [x[0] for x in data1]
	output.append(staff)

	# revenue per staff
	queryv   =   "select (revenue_banking+revenue_bills+revenue_fx+revenue_insurance+revenue_investments+revenue_loans+revenue_service_loans)/(numstaff_customer_facing_sales+numstaff_customer_facing_service+numstaff_management_activities+numstaff_non_customer_facing_general_admin+numstaff_non_customer_facing_sales_admin+numstaff_non_customer_facing_service+numstaff_staff_help+numstaff_training) from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	revenue  =   [str(round(x[0],2)) for x in data1]
	output.append(revenue)

	# cost per staff
	queryv   =   "select (cost_banking+cost_bills+cost_fx+cost_insurance+cost_investments+cost_loans+cost_service_loans)/(numstaff_customer_facing_sales+numstaff_customer_facing_service+numstaff_management_activities+numstaff_non_customer_facing_general_admin+numstaff_non_customer_facing_sales_admin+numstaff_non_customer_facing_service+numstaff_staff_help+numstaff_training) from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	cost     =   [str(round(x[0],2)) for x in data1]
	output.append(cost)

	# offer score
	queryv   =   "select round(((atm_with_audio_access+atm_with_deposit_facility+atm_with_disabled_access+atm_without_deposit_facility+banking_credit_cards+banking_internet_banking+banking_savings_accounts+banking_term_deposits+banking_transaction_accounts+banking_product_campaign_in_the_last_week+digital_information_screens+financial_planner+insurance+internet_banking_kiosk+investments_managed_funds+investments_nab_trade+investments_superannuation+leisure_zone_coffee_couches_kids_play_zone+loans_home_loans+loans_personal_loans+mobile_banker+quick_change_machine+small_business_banker)/23)*100) from  nab.currentstate_branchperformance_parramatta;"
	data1    =   Helpers.runFetch(Globals.db,queryv,{})
	offer    =   [str(round(x[0],2)) for x in data1]
	output.append(offer)

	output 	 =	 np.array(output).transpose()
	output 	 =	 [list(x) for x in output]
	#print (data)

	return json.dumps(Helpers.arrToColumns(output,['Branch-id','Community Score','Branch NPS','Ave waiting times','Foot traffic','Total staff','Revenue per staff','Cost per staff','Offer score']))

########
#
# lens 3
#
########

# Lens 3 (chart 11) - column 1 chart 1	
@app.route('/branch/currentsegment')
def getBranchBySegmentCurrent():
	query = """
		select segment1, segment2,segment3,segment4,segment5 
		from currentstate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{})
	x_data = [
		'Young and adventurous',
		'Professional',
		'Retired couple',
		'Young couple',
		'Couple with kids'
		]

	return json.dumps({'data': [
		['x'] + x_data,
		['Number of Customer'] + [float(value) for value in data[0]]
	]})

# Lens 3 (chart 12) - column 1 chart 2
@app.route('/branch/currentfoottraffic')
def getBranchFootTrafficCurrent():
	query = """
		select cast(foot_traffic_banking/1000 as unsigned),
				cast(foot_traffic_bills/1000 as unsigned),
				cast(foot_traffic_fx/1000 as unsigned),
				cast(foot_traffic_insurance/1000 as unsigned),
				cast(foot_traffic_investments/1000 as unsigned),
				cast(foot_traffic_loans/1000 as unsigned),
				cast(foot_traffic_service_loans/1000 as unsigned)
		from currentstate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{})
	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]

	return json.dumps({'data': [
		['x'] + x_data,
		['Transaction'] + [value for value in data[0]]
	]})


# Lens 3 (chart 13) - column 1 chart 3
@app.route('/branch/currentrevenue')
def getBranchRevenueCurrent():
	query = """
		select revenue_banking/1000,
				revenue_bills/1000,
				revenue_fx/1000,
				revenue_insurance/1000,
				revenue_investments/1000,
				revenue_loans/1000,
				revenue_service_loans/1000
		from currentstate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{}) # an array: revenue break down
	
	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]
		
	return json.dumps({'data': [
		['x'] + x_data,
		['Revenue'] + [round(float(value),2) for value in data[0]]
	]})

# Lens 3 (chart 14) - column 1 table 1
@app.route('/branch/currentsummary')
def getBranchSummaryCurrent():
	query_foottraffic = """
		select cast((foot_traffic_banking + 
				foot_traffic_bills + 
				foot_traffic_fx + 
				foot_traffic_insurance + 
				foot_traffic_investments + 
				foot_traffic_loans + 
				foot_traffic_service_loans)/3650 as unsigned)
		from currentstate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	query_revenue = """
		select (revenue_banking + 
				revenue_bills + 
				revenue_fx + 
				revenue_insurance + 
				revenue_investments + 
				revenue_loans + 
				revenue_service_loans)/1000000
		from currentstate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	query_cost = """
		select cost/1000000 
		from currentstate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	query_profit = """
		select ((revenue_banking + 
				revenue_bills + 
				revenue_fx + 
				revenue_insurance + 
				revenue_investments + 
				revenue_loans + 
				revenue_service_loans) - cost)/1000000
		from currentstate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	
	foottraffic=Helpers.runFetch(Globals.db,query_foottraffic,{}) # one value: daily foot traffic of a branch
	revenue=Helpers.runFetch(Globals.db,query_revenue,{}) # one value
	cost=Helpers.runFetch(Globals.db,query_cost,{}) # one value
	profit=Helpers.runFetch(Globals.db,query_profit,{}) # one value
	
	# here we're formating the data in a transposed format and then we will change css to let the 1st column as the header
	return json.dumps([
		['Daily Foot Traffic', 'Revenue', 'Cost', 'profit'],
		[ 
			str(foottraffic[0][0]),
			'$ ' + str(round(float(revenue[0][0]),4)) + 'mil',
			'$ ' + str(round(float(cost[0][0]),4)) + 'mil',
			'$ ' + str(round(float(profit[0][0]),4)) + 'mil'
		]
	])
	

# Lens 3 (chart 21) - column 2 chart 1	
@app.route('/branch/futuresegment')
def getBranchBySegmentFuture():
	query = """
		select segment1, segment2,segment3,segment4,segment5 
		from futurestate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{})
	x_data = [
		'Young and adventurous',
		'Professional',
		'Retired couple',
		'Young couple',
		'Couple with kids'
		]

	return json.dumps({'data': [
		['x'] + x_data,
		['Number of Customer'] + [float(value) for value in data[0]]
	]})

# Lens 3 (chart 22) - column 2 chart 2
@app.route('/branch/futurefoottraffic')
def getBranchFootTrafficFuture():
	query = """
		select cast(foot_traffic_banking/1000 as unsigned),
				cast(foot_traffic_bills/1000 as unsigned),
				cast(foot_traffic_fx/1000 as unsigned),
				cast(foot_traffic_insurance/1000 as unsigned),
				cast(foot_traffic_investments/1000 as unsigned),
				cast(foot_traffic_loans/1000 as unsigned),
				cast(foot_traffic_service_loans/1000 as unsigned)
		from futurestate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{})
	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]

	return json.dumps({'data': [
		['x'] + x_data,
		['Transaction'] + [value for value in data[0]]
	]})


# Lens 3 (chart 23) - column 2 chart 3
@app.route('/branch/futurerevenue')
def getBranchRevenueFuture():
	query = """
		select revenue_banking/1000,
				revenue_bills/1000,
				revenue_fx/1000,
				revenue_insurance/1000,
				revenue_investments/1000,
				revenue_loans/1000,
				revenue_service_loans/1000
		from futurestate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	data=Helpers.runFetch(Globals.db,query,{}) # an array: revenue break down
	
	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]
		
	return json.dumps({'data': [
		['x'] + x_data,
		['Revenue'] + [round(float(value),2) for value in data[0]]
	]})

# Lens 3 (chart 24) - column 2 table 1
@app.route('/branch/futuresummary')
def getBranchSummaryFuture():
	query_foottraffic = """
		select cast((foot_traffic_banking + 
				foot_traffic_bills + 
				foot_traffic_fx + 
				foot_traffic_insurance + 
				foot_traffic_investments + 
				foot_traffic_loans + 
				foot_traffic_service_loans)/3650 as unsigned)
		from futurestate_branchperformance_parramatta 
		where branch_id = '082-228'
	"""
	query_revenue = """
		select (revenue_banking + 
				revenue_bills + 
				revenue_fx + 
				revenue_insurance + 
				revenue_investments + 
				revenue_loans + 
				revenue_service_loans)/1000000
		from futurestate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	query_cost = """
		select cost/1000000 
		from futurestate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	query_profit = """
		select ((revenue_banking + 
				revenue_bills + 
				revenue_fx + 
				revenue_insurance + 
				revenue_investments + 
				revenue_loans + 
				revenue_service_loans) - cost)/1000000
		from futurestate_branchperformance_parramatta
		where branch_id = '082-228'
	"""
	
	foottraffic=Helpers.runFetch(Globals.db,query_foottraffic,{}) # one value: daily foot traffic of a branch
	revenue=Helpers.runFetch(Globals.db,query_revenue,{}) # one value
	cost=Helpers.runFetch(Globals.db,query_cost,{}) # one value
	profit=Helpers.runFetch(Globals.db,query_profit,{}) # one value
	
	# here we're formating the data in a transposed format and then we will change css to let the 1st column as the header
	return json.dumps([
		['Daily Foot Traffic', 'Revenue', 'Cost', 'profit'],
		[ 
			str(foottraffic[0][0]),
			'$ ' + str(round(float(revenue[0][0]),4)) + 'mil',
			'$ ' + str(round(float(cost[0][0]),4)) + 'mil',
			'$ ' + str(round(float(profit[0][0]),4)) + 'mil'
		]
	])

# Lens 3 (chart 31)
@app.route('/branch/optimisedsegment')
def getBranchBySegmentOptimised():
	query = None
	chosen = request.args.get('chosen', None)
	
	if chosen == 'location':
		query = """
			select segment1, segment2,segment3,segment4,segment5 
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
	if chosen == 'product':
		query = """
			select segment1, segment2,segment3,segment4,segment5 
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""	
	if chosen == 'branch':
		query = """
			select segment1, segment2,segment3,segment4,segment5 
			from currentstate_branchperf_optimised_facmix_parramatta
			where branch_id = '082-228'
		"""

	'''
	x_data = [
		'segment 1',
		'segment 2',
		'segment 3',
		'segment 4',
		'segment 5'
		]
	'''

	x_data = [
		'Young and adventurous',
		'Professional',
		'Retired couple',
		'Young couple',
		'Couple with kids'
		]
	
	if query is not None:
		data=Helpers.runFetch(Globals.db,query,{})
		return json.dumps({'data': [
			['x'] + x_data,
			['Number of Customer'] + [round(float(value)*(random.randint(90,110)/100.0)) for value in data[0]] ##### need to change
		]})
	else:
		return json.dumps({'data': [
			['x'] + x_data,
			['Number of Customer'] + [0,0,0,0,0]
		]})


# Lens 3 (chart 32)
@app.route('/branch/optimisedfoottraffic')
def getBranchFootTrafficOptimised():
	query = None
	chosen = request.args.get('chosen', None)
	
	if chosen == 'location':
		query = """
			select cast(foot_traffic_banking/1000 as unsigned),
				cast(foot_traffic_bills/1000 as unsigned),
				cast(foot_traffic_fx/1000 as unsigned),
				cast(foot_traffic_insurance/1000 as unsigned),
				cast(foot_traffic_investments/1000 as unsigned),
				cast(foot_traffic_loans/1000 as unsigned),
				cast(foot_traffic_service_loans/1000 as unsigned)
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
	if chosen == 'product':
		query = """
			select cast(foot_traffic_banking/1000 as unsigned),
				cast(foot_traffic_bills/1000 as unsigned),
				cast(foot_traffic_fx/1000 as unsigned),
				cast(foot_traffic_insurance/1000 as unsigned),
				cast(foot_traffic_investments/1000 as unsigned),
				cast(foot_traffic_loans/1000 as unsigned),
				cast(foot_traffic_service_loans/1000 as unsigned)
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""	
	if chosen == 'branch':
		query = """
			select cast(foot_traffic_banking/1000 as unsigned),
				cast(foot_traffic_bills/1000 as unsigned),
				cast(foot_traffic_fx/1000 as unsigned),
				cast(foot_traffic_insurance/1000 as unsigned),
				cast(foot_traffic_investments/1000 as unsigned),
				cast(foot_traffic_loans/1000 as unsigned),
				cast(foot_traffic_service_loans/1000 as unsigned)
			from currentstate_branchperf_optimised_facmix_parramatta
			where branch_id = '082-228'
		"""

	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]
	
	if query is not None:
		data=Helpers.runFetch(Globals.db,query,{})
		return json.dumps({'data': [
			['x'] + x_data,
			['Transaction'] + [round(float(value)*(random.randint(90,110)/100.0)) for value in data[0]] #### change random
		]})
	else:
		return json.dumps({'data': [
			['x'] + x_data,
			['Transaction'] + [0,0,0,0,0]
		]})


# Lens 3 (chart 33)
@app.route('/branch/optimisedrevenue')
def getBranchRevenueOptimised():
	query = None
	chosen = request.args.get('chosen', None)
	
	if chosen == 'location':
		query = """
			select revenue_banking/1000,
				revenue_bills/1000,
				revenue_fx/1000,
				revenue_insurance/1000,
				revenue_investments/1000,
				revenue_loans/1000,
				revenue_service_loans/1000
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
	if chosen == 'product':
		query = """
			select revenue_banking/1000,
				revenue_bills/1000,
				revenue_fx/1000,
				revenue_insurance/1000,
				revenue_investments/1000,
				revenue_loans/1000,
				revenue_service_loans/1000
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""	
	if chosen == 'branch':
		query = """
			select revenue_banking/1000,
				revenue_bills/1000,
				revenue_fx/1000,
				revenue_insurance/1000,
				revenue_investments/1000,
				revenue_loans/1000,
				revenue_service_loans/1000
			from currentstate_branchperf_optimised_facmix_parramatta
			where branch_id = '082-228'
		"""

	x_data = [
		'banking',
		'bills',
		'fx',
		'insurance',
		'investments',
		'loans',
		'service loans'
		]
	
	if query is not None:
		data=Helpers.runFetch(Globals.db,query,{})
		return json.dumps({'data': [
			['x'] + x_data,
			['Revenue'] + [round(float(value)**(random.randint(90,110)/100.0)) for value in data[0]]   ### change random
		]})
	else:
		return json.dumps({'data': [
			['x'] + x_data,
			['Revenue'] + [0,0,0,0,0]
		]})

# Lens 3 (chart 34) 
@app.route('/branch/optimisedsummary')
def getBranchSummaryOptimised():
	query_foottraffic = None
	query_revenue = None
	query_cost = None
	query_profit = None
	chosen = request.args.get('chosen', None)
	
	if chosen == 'location':
		query_foottraffic = """
			select cast((foot_traffic_banking + 
					foot_traffic_bills + 
					foot_traffic_fx + 
					foot_traffic_insurance + 
					foot_traffic_investments + 
					foot_traffic_loans + 
					foot_traffic_service_loans)/3650 as unsigned)
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
		query_revenue = """
			select (revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans)/1000000
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
		query_cost = """
			select cost/1000000 
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
		query_profit = """
			select ((revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans) - cost)/1000000
			from currentstate_branchperf_optimised_drivetime_parramatta 
			where branch_id = '082-228'
		"""
	if chosen == 'product':
		query_foottraffic = """
			select cast((foot_traffic_banking + 
					foot_traffic_bills + 
					foot_traffic_fx + 
					foot_traffic_insurance + 
					foot_traffic_investments + 
					foot_traffic_loans + 
					foot_traffic_service_loans)/365 as unsigned)
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""
		query_revenue = """
			select (revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans)/1000000
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""
		query_cost = """
			select cost/1000000 
			from currentstate_branchperf_optimised_prodmix_parramatta 
			where branch_id = '082-228'
		"""
		query_profit = """
			select ((revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans) - cost)/1000000
			from currentstate_branchperf_optimised_prodmix_parramatta 
			where branch_id = '082-228'
		"""	
	if chosen == 'branch':
		query_foottraffic = """
			select cast((foot_traffic_banking + 
					foot_traffic_bills + 
					foot_traffic_fx + 
					foot_traffic_insurance + 
					foot_traffic_investments + 
					foot_traffic_loans + 
					foot_traffic_service_loans)/365 as unsigned)
			from currentstate_branchperf_optimised_facmix_parramatta
			where branch_id = '082-228'
		"""
		query_revenue = """
			select (revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans)/1000000
			from currentstate_branchperf_optimised_prodmix_parramatta
			where branch_id = '082-228'
		"""
		query_cost = """
			select cost/1000000 
			from currentstate_branchperf_optimised_facmix_parramatta 
			where branch_id = '082-228'
		"""
		query_profit = """
			select ((revenue_banking + 
					revenue_bills + 
					revenue_fx + 
					revenue_insurance + 
					revenue_investments + 
					revenue_loans + 
					revenue_service_loans) - cost)/1000000
			from currentstate_branchperf_optimised_facmix_parramatta 
			where branch_id = '082-228'
		"""	
	
	if chosen is not None:
		foottraffic=Helpers.runFetch(Globals.db,query_foottraffic,{}) # one value: daily foot traffic of a branch
		revenue=Helpers.runFetch(Globals.db,query_revenue,{}) # one value
		cost=Helpers.runFetch(Globals.db,query_cost,{}) # one value
		profit=Helpers.runFetch(Globals.db,query_profit,{}) # one value
		return json.dumps([
		['Daily Foot Traffic', 'Revenue', 'Cost', 'profit'],
		[ 
			str(foottraffic[0][0]),
			'$ ' + str(round(float(revenue[0][0])*(random.randint(90,110)/100.0),4)) + 'mil',   ### change random
			'$ ' + str(round(float(cost[0][0])*(random.randint(90,110)/100.0),4)) + 'mil',
			'$ ' + str(round(float(profit[0][0])*(random.randint(90,110)/100.0),4)) + 'mil'
		]
	])
	else:
		return json.dumps([
		['Daily Foot Traffic', 'Revenue', 'Cost', 'profit'],
		[0,0,0,0]
	])


# Lens 3 (chart 41)
@app.route('/branch/facility')
def getBranchFacility():
	query =  None
	chosen = request.args.get('chosen', None)

	#
	# current
	#
	query   =   "SELECT * FROM nab.currentstate_branchperformance_parramatta where branch_id = '082-228'"
	data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

	query   =   "show columns from nab.currentstate_branchperformance_parramatta"
	data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

	dadict  =   {}

	for i in range(len(data2)):

		dadict[data2[i][0]]=data1[0][i]

	datacu=Helpers.branch_facilities(data1,dadict,'branch')
	datat=Helpers.branch_facilities(data1,dadict,'waiting')
	datacu.extend(datat)
	datac=[]


	#
	#  optimised
	#
	if chosen == 'location':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_drivetime_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_drivetime_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'branch')
		datat=Helpers.branch_facilities(data1,dadict,'waiting')
		datac.extend(datat)

	if chosen == 'product':


		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_prodmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_prodmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'branch')
		datat=Helpers.branch_facilities(data1,dadict,'waiting')
		datac.extend(datat)

	if chosen == 'branch':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_facmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_facmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'branch')
		datat=Helpers.branch_facilities(data1,dadict,'waiting')
		datac.extend(datat)




	#
	# merging
	#
	datamerged=[]

	for i in range(len(datac)):

		datacu[i].extend([datac[i][1]])
		datamerged.append(datacu[i])

	if len(datac)==0:

		datamerged=datacu



	return json.dumps(Helpers.arrToColumns(datamerged,['Parameter','Current','Optimised']))


# Lens 3 (chart 332)
@app.route('/branch/staff')
def getbranchstaff():
	query =  None
	chosen = request.args.get('chosen', None)

	
	if chosen == 'location':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_drivetime_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_drivetime_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'staff')

	if chosen == 'product':


		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_prodmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_prodmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'staff')

	if chosen == 'branch':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_facmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_facmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'staff')


	# current
	queryv   =   "SELECT * FROM nab.currentstate_branchperformance_parramatta where branch_id = '082-228'"
	data1   =   Helpers.runFetch(Globals.db,queryv,{}) # one value: daily foot traffic of a branch

	queryv   =   "show columns from nab.currentstate_branchperformance_parramatta"
	data2   =   Helpers.runFetch(Globals.db,queryv,{}) # one value: daily foot traffic of a branch

	dadict  =   {}

	for i in range(len(data2)):

		dadict[data2[i][0]]=data1[0][i]

	datacu=Helpers.branch_facilities(data1,dadict,'staff')


	x_data	=	[x[0] for x in datacu]

	if query is not None:

		return json.dumps({'data': [
			['x'] + x_data,
			['skill mix (current)'] + [float(value[1]) for value in datacu],
			['skill mix (optimised)'] + [round(float(value[1]*random.randint(70,90)/100)) for value in datac]
		]})
	else:
		return json.dumps({'data': [
			['x'] + x_data,
			['skill mix (current)'] + [float(value[1]) for value in datacu]
		]})

# Lens 3 (chart 332)
@app.route('/branch/skills')
def getbranchskills():
	query =  None
	datac = []
	chosen = request.args.get('chosen', None)


	if chosen == 'location':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_drivetime_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_drivetime_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'skills')

	if chosen == 'product':


		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_prodmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_prodmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'skills')

	if chosen == 'branch':

		query   =   "SELECT * FROM nab.currentstate_branchperf_optimised_facmix_parramatta where branch_id = '082-228'"
		data1   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		query   =   "show columns from nab.currentstate_branchperf_optimised_facmix_parramatta"
		data2   =   Helpers.runFetch(Globals.db,query,{}) # one value: daily foot traffic of a branch

		dadict  =   {}

		for i in range(len(data2)):

			dadict[data2[i][0]]=data1[0][i]

		datac=Helpers.branch_facilities(data1,dadict,'skills')


	# current
	queryv   =   "SELECT * FROM nab.currentstate_branchperformance_parramatta where branch_id = '082-228'"
	data1   =   Helpers.runFetch(Globals.db,queryv,{}) # one value: daily foot traffic of a branch

	queryv   =   "show columns from nab.currentstate_branchperformance_parramatta"
	data2   =   Helpers.runFetch(Globals.db,queryv,{}) # one value: daily foot traffic of a branch

	dadict  =   {}

	for i in range(len(data2)):

		dadict[data2[i][0]]=data1[0][i]

	datacu=Helpers.branch_facilities(data1,dadict,'skills')

	datamerged=[]

	for i in range(len(datacu)):

		if len(datac)>0:

			datacu[i].extend([datac[i][1]])

		datamerged.append(datacu[i])

	print (datamerged)

	if len(datac)==0:

		return json.dumps(Helpers.arrToColumns(datamerged,['Parameter','Current']))

	else:

		return json.dumps(Helpers.arrToColumns(datamerged,['Parameter','Current','Optimised']))


'''

# Lens 3 (chart 42)
@app.route('/branch/employee')
def getBranchEmployee():
	query_optimised = None
	chosen = request.args.get('chosen', None)
	
	if chosen == 'location':
		query_optimised = """
			select numstaff_customer_facing_sales + 1,
					numstaff_customer_facing_service,
					numstaff_management_activities + 1,
					numstaff_non_customer_facing_general_admin,
					numstaff_non_customer_facing_sales_admin -2,
					numstaff_non_customer_facing_service,
					numstaff_staff_help,
					numstaff_training
			from currentstate_branchperf_optimised_drivetime_parramatta
		"""
	if chosen == 'product':
		query_optimised = """
			select numstaff_customer_facing_sales + 2,
					numstaff_customer_facing_service,
					numstaff_management_activities,
					numstaff_non_customer_facing_general_admin,
					numstaff_non_customer_facing_sales_admin,
					numstaff_non_customer_facing_service -1,
					numstaff_staff_help -1,
					numstaff_training
			from currentstate_branchperf_optimised_prodmix_parramatta
		"""
	if chosen == 'branch':
		query_optimised = """
			select numstaff_customer_facing_sales,
					numstaff_customer_facing_service + 1,
					numstaff_management_activities,
					numstaff_non_customer_facing_general_admin - 1,
					numstaff_non_customer_facing_sales_admin,
					numstaff_non_customer_facing_service,
					numstaff_staff_help,
					numstaff_training
			from currentstate_branchperf_optimised_facmix_parramatta
		"""
	query_current = """
		select numstaff_customer_facing_sales,
					numstaff_customer_facing_service,
					numstaff_management_activities,
					numstaff_non_customer_facing_general_admin,
					numstaff_non_customer_facing_sales_admin,
					numstaff_non_customer_facing_service,
					numstaff_staff_help,
					numstaff_training
		from currentstate_branchperformance_parramatta
	"""
	
	current=Helpers.runFetch(Globals.db,query_current,{})
	
	x_data = [
		'customer facing sales',
		'customer facing service',
		'management activities',
		'general admin',
		'sales admin',
		'other service',
		'staff help',
		'training'
	]
	
	if query_optimised is not None:
		optimised=Helpers.runFetch(Globals.db,query_optimised,{})
		return json.dumps({'data': [
			['x'] + x_data,
			['skill mix (current)'] + [float(value) for value in current[0]],
			['skill mix (optimised)'] + [float(value) for value in optimised[0]]
		]})
	else:
		return json.dumps({'data': [
			['x'] + x_data,
			['skill mix (current)'] + [float(value) for value in current[0]]
		]})
'''
######## START LENS FOUR #######

	
######## START DISPLAY ONE - TABLE #########
@app.route('/branch/display1')
def getbranchdisplay1():
	chosen = request.args.get('chosen', None)
	query = """
			select Address, Suburb, State, Profit
			from national_prescriptive_profit_Display_1
			"""
				
	data=Helpers.runFetch(Globals.db,query,{})	
	data=Helpers.arrToColumns(data,['Branch Address','Suburb','State','2017 Profit'])
	
	if chosen == 'national':
		query = """
				select Address, Suburb, State, Profit
				from national_prescriptive_profit_Display_1
				"""
				
		data=Helpers.runFetch(Globals.db,query,{})	
		data=Helpers.arrToColumns(data,['Branch Address','Suburb','State','2017 Profit'])			
		
			
	if chosen == 'regional':
		query = """
				select Address, Postcode, Total_Staff, Column_2017_Profit
				from regional_descriptive_branches_Display_1
				"""
				
		data=Helpers.runFetch(Globals.db,query,{})	
		data=Helpers.arrToColumns(data,['Branch Address','Postcode','Total Staff','2017 Profit'])			
		
			
	if chosen == 'branch':
		query = """
			select Paramatta_Branch_Network_state,
				Profit,
				Revenue,
				Costs,
				Foot_Traffic,
				Avg_wait_time
				from branch_prescriptive_demos_Display_1
			"""
			
		data=Helpers.runFetch(Globals.db,query,{})	
		data=Helpers.arrToColumns(data,['Branch State','Profit','Revenue','Costs', 'Foot Traffic', 'Avg wait time'])	
		
	return json.dumps(data)
	
	
	
######## END DISPLAY ONE - TABLE #########


######## START DISPLAY TWO - BARCHART #########
@app.route('/branch/display2')
def getbranchdisplay2():
	chosen = request.args.get('chosen', None)
	
	query = """
			select x, Column_0_200k, Column_200k_300k, Column_300k_400k, Column_400k_500k, Column_500k_600k, Column_600k_700k, Column_700k
			from national_prescriptive_profit_Display_2
			"""
			
	data=Helpers.runFetch(Globals.db,query,{})
	data=Helpers.arrToColumns(data,['x', '$0-$200k', '$200k-$300k', '$300k-$400k', '$400k-$500k', '$500k-$600k', '$600k-$700k', '$700k+'])	
	
	if chosen == 'national':
		query = """
				select x, Column_0_200k, Column_200k_300k, Column_300k_400k, Column_400k_500k, Column_500k_600k, Column_600k_700k, Column_700k
				from national_prescriptive_profit_Display_2
			"""
			
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x', '$0-$200k', '$200k-$300k', '$300k-$400k', '$400k-$500k', '$500k-$600k', '$600k-$700k', '$700k+'])	
		
		
		
	if chosen == 'regional':
		query = """
			select x, ATM_deposit_facility, Coin_swap_machine, Tablets_for_customer_use, Internet_banking_kiosk, Leisure_zones, Electronic_queuing_systems, Quick_change_machine
			from regional_descriptive_branches_Display_2
			"""
		
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x','ATM with deposit', 'Coin swap machine', 'Tablets for customers', 'Internet banking kiosk', 'Leisure zones', 'Electronic queuing system', 'Quick change machine'])	
		
		
	if chosen == 'branch':
		query = """
			select x,Profit/1000000,Revenue/1000000,Cost/1000000
			from branch_prescriptive_demos_Display_2
			"""
		
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x','profit','revenue','cost'])	
	
	return json.dumps({'data': data})
		
	
	
######## END DISPLAY TWO - BARCHART #########	


	
######## START DISPLAY THREE - MAP MARKERS #########

@app.route('/branch/display3')
def getbranchdisplay3():
	query= """SELECT branch_id, bank, address, CONCAT(`longitude`),CONCAT(latitude) FROM branch_geo WHERE latitude IN (SELECT latitude FROM national_prescriptive_profit_Display_3)  """;
	chosen = request.args.get('chosen', None)
	
	if chosen == 'national':
		query= """SELECT branch_id, bank, address, CONCAT(`longitude`),CONCAT(latitude) FROM branch_geo WHERE latitude IN (SELECT latitude FROM national_prescriptive_profit_Display_3)  """;
		
	if chosen == 'regional':
		query= """SELECT branch_id, bank, address, CONCAT(`longitude`),CONCAT(latitude) FROM branch_geo WHERE latitude IN (SELECT latitude FROM regional_descriptive_branches_Display_3)  """;
		
	if chosen == 'branch': 
		query= """SELECT branch_id, bank, address, CONCAT(`longitude`),CONCAT(latitude) FROM branch_geo WHERE latitude IN (SELECT latitude FROM branch_prescriptive_demos_Display_3)  """;
	
	args={}
	clause=''
	query_data=request.args.get('banks')
	
	if(query_data):
		query_data=query_data.split(',')
		clause=" bank='"+" OR bank='".join(query_data)+"'"
		query='SELECT branch_id, bank, address, CONCAT(`longitude`),CONCAT(latitude) FROM branch_geo WHERE '+clause+' LIMIT 100'
		
	data=Helpers.runMapFetch(Globals.db,query,{},['branch_id','bank','address','longitude','latitude'])
		
	return json.dumps(data)
	
######## END DISPLAY THREE - MAP MARKERS #########	


######## START DISPLAY FOUR - BAR CHART #########

@app.route('/branch/display4')
def getbranchdisplay4():
	chosen = request.args.get('chosen', None)
	
	query = """
			select x, Profit/1000000, Revenue/1000000, Costs/1000000
			from national_prescriptive_profit_Display_4
			"""
			
	data=Helpers.runFetch(Globals.db,query,{})
	data=Helpers.arrToColumns(data,['x','Profit', 'Revenue', 'Costs'])
	
	if chosen == 'national':
		query = """
				select x, Profit/1000000, Revenue/1000000, Costs/1000000
				from national_prescriptive_profit_Display_4
			"""
			
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x','Profit', 'Revenue', 'Costs'])
		
		
	if chosen == 'regional':
		query = """
				select x, Extended_Store_Hours, Financial_Planner, Managed_Investment_Funds, Small_Business_Banker, NAB_Trade_Investment, Superannuation, Foreign_Exchange
				from regional_descriptive_branches_Display_4
			"""
			
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x','Extended Hours', 'Financial Planner', 'Managed Investment Fund', 'Small Business Banker', 'NAB Trade', 'Superannuation', 'Foreign Exhange'])
		
		
	if chosen == 'branch':
		query = """
			select x, Current_State, Opt_drive_time, Opt_fac_prod_mix, Opt_product_mix,Opt_facility_mix
			from branch_prescriptive_demos_Display_4
		"""
		
		data=Helpers.runFetch(Globals.db,query,{})
		data=Helpers.arrToColumns(data,['x','Current State', 'Opt. Drive Time', 'Opt. Fac. & Prod. Mix', 'Opt. Product Mix', 'Opt. Facility Mix'])
	
	return json.dumps({'data': data})



######## END DISPLAY FOUR - BAR CHART #########	



@app.route('/branch/display1title')
def getbranchdisplaytitl1():	
	text = 'abcdefghijklmop'

	return text

#################### END LENS 4
	
################################## APP #######################################
	
if __name__ == "__main__":
	#app.run(host='0.0.0.0',port=80)  ##### switch back
	app.run(debug=True)
